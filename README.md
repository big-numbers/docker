## Quick start 
    # Copy .env file
    cp .env.dev .env

    # Start docker containers needed for working back
    docker-compose up -d nginx php-fpm postgres workspace
